import React from 'react';
import Footer from './Footer';
import './about.css';

class About extends React.Component {
   
    render() {
        return (
            <div>
            <div className='about-container'>
                <div className='about-us-text'>
                    <div className='heading'>About Us</div>
                    <div className='inner-text'><h2>Lorem ipsum dolor sit amet</h2> Consectetur adipiscing elit. Phasellus id felis odio.
                        Duis ac arcu vitae velit hendrerit commodo at a mauris. Donec volutpat dignissim suscipit. Nunc ac tellus ut eros
                        cursus commodo ut id felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean commodo nulla vel
                        velit semper, a hendrerit arcu euismod. Sed in ligula iaculis, condimentum nibh quis, pellentesque eros. </div>
                </div>
            </div>
                <Footer/>
            </div>

        )
    }
}
export default About;
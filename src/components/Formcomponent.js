import React from 'react';
import './form.css';
import validator from 'validator';
// import { Form, nput, Label, button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


class Formcomponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: {
                name: null,
                email: null,
                age: null,
                password: null,
                confirmPassword: null,
                checkbox: false

            },
            errorValue: {
                nameError: null,
                emailError: null,
                ageError: null,
                passwordError: null,
                confirmPasswordError: null,
                checkboxError: null
            },
            formStatus: {
                status: 'notsubmitted',
                message: null
            }
        }
    }


    handleChange = (e) => {
        const { inputValue, errorValue } = { ...this.state };

        errorValue[e.target.id + 'Error'] = null;

        if (e.target.type === "checkbox") {
            inputValue[e.target.id] = !inputValue.checkbox;
        }
        else {
            inputValue[e.target.id] = e.target.value;
        }

        this.setState({ inputValue, errorValue });
    }

    nameValidate = (value) => {
        
        if (value === null) {
            return "Enter a valid name";
        }
        let len = value.trim().length;
        if (len == 0) {
            return "Enter a valid name";
        } else {
            if (!validator.isLength(value, { min: 2, max: 50 })) {
                return "Name must contain more than 2 chatacters";
            }
            if (!validator.isAlpha(value, "en-US", { ignore: " -" })) {
                return `Name must only contain letters, space and "-"`;
            }
        }


        return 0;
    }

    emailValidate = (value) => {
        if (value === null) {
            return "Enter a valid email";
        }
        if (validator.isEmail(value, { blacklisted_chars: '~`!#$%^&*()' }) === false) {
            return "Enter a valid email";
        }
        return 0;
    }

    ageValidate = (value) => {
        if (value === null) {
            return 'Enter a valid age';
        }
        if (validator.isInt(value, { min: 16, max: 130, allow_leading_zeroes: false }) === false) {
            return 'Age should be between 16 to 130';
        }
        return 0;
    }

    passwordValidate = (value) => {
        if (value === null) {
            return "Password is missing";
        }

        if (value.includes(' ')) {
            return "Password should not contain space";
        } else if (validator.isStrongPassword(value, { minLength: 8, minUppercase: 1, minNumbers: 1, minSymbols: 1 }) === false) {
            return "Password must contain minimun 8 characters, uppercase:1, symbol:1, number:1";
        }

        return 0;
    }

    confirmPasswordValidate = (password, value) => {
        if (value === null) {
            return 'Confirm your Password';
        }

        if (validator.equals(password, value) === false) {
            return 'Password does not match';
        }
        return 0;
    }

    checkboxValidate = (value) => {

        if (value === false) {
            return "Agree to the terms and conditions";
        }
        return 0;

    }

    validateForm = (value) => {
        const { inputValue, errorValue } = { ...this.state };
        let emessage = 0;

        switch (value) {

            case "name": let name = this.nameValidate(inputValue['name']);
                if (name !== 0) {
                    emessage = 1;
                    errorValue.nameError = name;
                }
                break;

            case "email": let email = this.emailValidate(inputValue['email']);
                if (email !== 0) {
                    emessage = 1;
                    errorValue.emailError = email;
                }
                break;

            case "age":
                let age = this.ageValidate(inputValue['age']);
                if (age !== 0) {
                    emessage = 1;
                    errorValue.ageError = age;
                }
                break;

            case "password": let password = this.passwordValidate(inputValue['password']);
                if (password !== 0) {
                    emessage = 1;
                    errorValue.passwordError = password;
                }
                break;

            case "confirmPassword": let confirmPassword = this.confirmPasswordValidate(inputValue['password'], inputValue['confirmPassword']);
                if (confirmPassword !== 0) {
                    emessage = 1;
                    errorValue.confirmPasswordError = confirmPassword;
                }
                break;

            case "checkbox": let checkbox = this.checkboxValidate(inputValue['checkbox']);
                if (checkbox !== 0) {
                    emessage = 1;
                    errorValue.checkboxError = checkbox;
                }
                break;

            default: break;
        }

        this.setState({ errorValue });
        return emessage;
    }

    handleSubmit = (e) => {
        e.preventDefault();

        const { formStatus } = { ...this.state };

        let checkValid = Object.keys(this.state.inputValue).map((element) => {
            return this.validateForm(element);
        })

        if (checkValid.includes(1)) {
            formStatus.status = 'notsubmitted';
            formStatus.message = null;
        } else {
            formStatus.status = 'submitted';
            formStatus.message = 'Form submitted successfully';
        }

        this.setState({ formStatus });
    }

    render() {
        return (
            <div  >
            
             <div >
                {this.state.formStatus.status === 'notsubmitted' ?
                    <div className="  main-container mt-4 container-md d-flex">
        
                    <div className="d-flex  align-items-center justify-content-center"><div className='red w-25  d-none d-sm-block'></div></div>
                    <div className='container w-75 d-flex'>
                        <form className="row w-100 form w-70  d-flex  justify-content-center col-xl-12" onSubmit={this.handleSubmit} >
                            <div className="col-md-4 input-div w-100 pt-4 d-flex-col mx-auto my-auto">
                            <div> <label htmlFor="name" className=" "> Full Name</label></div>
                            <div> <input type="text" className="" id="name" onChange={this.handleChange} placeholder='Alice Grey' /></div>
                               
                               
                                <div >
                                    <div className='errormessage'>{this.state.errorValue.nameError}</div>
                                </div>
                            </div>


                            <div className="col-md-4 input-div w-100 pt-4  d-flex-col mx-auto my-auto">
                            <div> <label htmlFor="email" className="">Email</label></div>
                            <div><input type="text" className=" w-85" id="email" onChange={this.handleChange} placeholder='alicegrey@gmail.com'/></div>
                               
                                
                                <div className="">
                                    <div className='errormessage'>{this.state.errorValue.emailError}</div>
                                </div>
                            </div>


                            <div className="col-md-4 input-div w-100 pt-4  d-flex-col mx-auto my-auto" >
                            <div><label htmlFor="age" className="form-label">Age</label></div>
                            <div> <input type="text" className="" id="age" onChange={this.handleChange} placeholder='29'/></div>
                                
                               
                                <div className="">
                                    <div className='errormessage'>{this.state.errorValue.ageError}</div>
                                </div>
                            </div>


                            <div className="col-md-4 input-div w-100 pt-4  d-flex-col mx-auto my-auto">
                            <div><label htmlFor="password" className="form-label">Password</label></div>
                            <div> <input type="password" className=" is-valid" id="password" onChange={this.handleChange} placeholder='Password'/></div>
                                
                                
                                <div className="">
                                    <div className='errormessage'>{this.state.errorValue.passwordError}</div>
                                </div>
                            </div>


                            <div className="col-md-4 input-div w-100 pt-4  d-flex-col mx-auto my-auto">
                            <div><label htmlFor="confirmPassword" className="form-label">Confirm Password</label></div>
                            <div>       <input type="password" className=" is-valid" id="confirmPassword" onChange={this.handleChange} placeholder='Confirm Password'/></div>
                                
                         
                                <div className="">
                                    <div className='errormessage'>{this.state.errorValue.confirmPasswordError}</div>
                                </div>
                            </div>






                            <div className="col-12 input-divw-100 pt-4 d-flex-col mx-auto my-auto">
                                <div className="d-flex-col">
                                <div className='checkbox d-flex justify-content-center align-items-center mx-auto py-auto'>
                                    <input className="" type="checkbox" value="" id="checkbox"  onClick={this.handleChange} />
                                    <label className="" htmlFor="checkbox">
                                        Agree to terms and conditions
                                    </label>
                                    </div>
                                    <div  className="">
                                        <div className='errormessage'>{this.state.errorValue.checkboxError}</div>
                                    </div>
                                </div>
                            </div>


                            <div className="col-12 button input-button d-flex mx-auto">
                                <button className="mx-auto w-50 py-2" type="submit">Sign Up</button>
                            </div>
                        </form>
                        </div>
                    </div> 
                    
                    : <div className="submitted mt-4" >
                        <h1 className="successmessage mt-5">{this.state.formStatus.message}</h1>
                        <ul>
                            <li>Name: {this.state.inputValue.name}</li>
                            <li>Email: {this.state.inputValue.email}</li>
                            <li>Age:{this.state.inputValue.age}</li>
                        </ul>
                    </div>}

                    </div>
            </div>
        )
    }

}

export default Formcomponent;


// <div className='main-container'>

// <div className="container">
//     <header className="App-header">Sign Up</header>
//     {this.state.formStatus.status === 'notsubmitted' ?
//         <div>

//             <form onSubmit={this.handleSubmit} className="form">
//                 <div>
//                     <label htmlFor="name">Name</label>
//                     <input type="text" id="name" onChange={this.handleChange} /><br />
//                     <div className='errormessage'>{this.state.errorValue.nameError}</div>
//                 </div>
//                 <div>
//                     <label htmlFor="email">Email</label>
//                     <input type="text" id="email" onChange={this.handleChange} /><br />
//                     <div className='errormessage'>{this.state.errorValue.emailError}</div>
//                 </div>
//                 <div>
//                     <label htmlFor="age">Age</label>
//                     <input type="number" id="age" onChange={this.handleChange} /><br />
//                     <div className='errormessage'>{this.state.errorValue.ageError}</div>
//                 </div>
//                 <div>
//                     <label htmlFor="password">Password</label>
//                     <input type="password" id="password" onChange={this.handleChange} /><br />
//                     <div className='errormessage'>{this.state.errorValue.passwordError}</div>
//                 </div>
//                 <div>
//                     <label htmlFor="confirmPassword">Confirm Password</label>
//                     <input type="password" id="confirmPassword" onChange={this.handleChange} /><br />
//                     <div className='errormessage'>{this.state.errorValue.confirmPasswordError}</div>
//                 </div>
//                 <div>
//                     <div className="checkbox">
//                         <input type="checkbox" id="checkbox" onClick={this.handleChange} />
//                         <span>Terms and Conditions</span>
//                         <div className='errormessage'>{this.state.errorValue.checkboxError}</div>
//                     </div>

//                 </div>
//                 <button >Submit</button>
//             </form>
//         </div>
//         : <div className="submitted" >
//             <h1 className="successmessage">{this.state.formStatus.message}</h1>
//             <ul>
//                 <li>Name: {this.state.inputValue.name}</li>
//                 <li>Email: {this.state.inputValue.email}</li>
//                 <li>Age:{this.state.inputValue.age}</li>
//             </ul>
//         </div>}
// </div>
// </div>
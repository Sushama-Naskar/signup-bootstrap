import React, { Component } from 'react';
import Footer  from './Footer';
import './home.css';


class Home extends Component {
   
    render() {
        return (
            <div>
            <div className='home-container'>
                <div className='home-us-text'>
                    <div className='home-heading'>Welcome</div>
                    <div className='home-inner-text'><h2>Lorem ipsum dolor sit amet</h2> Commodo at a mauris. Donec volutpat bhye dignissim suscipit. Nunc ac tellus ut eros
                        cursus commodo ut id felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean commodo nulla vel
                        velit semper, a hendrerit arcu euismod. Sed in ligula iaculis, condimentum nibh quis, pellentesque eros.</div>
                </div>
            </div>
            <Footer/>
            </div>
        )
    }
}

export default Home;

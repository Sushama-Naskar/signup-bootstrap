import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './footer.css';

class Footer extends Component {
   
    render() {
        return (
            <div className='footer-container'>
                    <div>Connect with us
                    <Link to="/Form" className='footer-item'>Sign Up</Link>
                    </div>
            </div>
        )
    }
}

export default Footer;